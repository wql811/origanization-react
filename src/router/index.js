import React, { Component } from 'react'

import { HashRouter, Switch, Route } from 'react-router-dom'

import home from '@/pages/home/home'
import login from '@/pages/login/login'

export default class RouteConfig extends Component {
    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route path='/' exact component={login} />
                    <Route path="/login" exact component={login}/>
                    <Route path="/home" exact component={home}/>
                </Switch>
            </HashRouter>
        )
    }
}