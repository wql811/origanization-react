import React, { Component } from 'react'
import { is, fromJS } from 'immutable'
class Home extends Component {
    state = {
        name: 'wql'
    }
    componentWillMount () {
        // 组件初始化时调用，更新不调用，整个生命周期只有一次，可以修改state
        this.setState({
            name: 'wql123'
        })
    }
    componentDidMount () {
        // 组件渲染之后调用，只能调用一次
        console.log('componentDidMount')
    }
    componentWillReceiveProps () {
        // 组件初始化不调用，组件接收新的props时调用
    }
    componentWillUpdate () {
        // 组件初始化时不调用，组件更新完成后调用，此时可以获取dom节点
    }
    componentWillUnmount () {
        // 组件将要卸载时调用，一些事件监听和定时器需要在此时清除
    }
    shouldComponentUpdate (nextProps, nextState) {
        // 组件判断是否需要重新渲染时调用
        // fromJS: 将纯 JS 对象和数组深层转换为不可变映射和列表
        // is: 判断是否值相等
        console.log(!is(fromJS(this.props),fromJS(nextProps)) || !is(fromJS(this.state), fromJS(nextState)))
        return !is(fromJS(this.props),fromJS(nextProps)) || !is(fromJS(this.state), fromJS(nextState))
    }
    deal = (val) => {
        console.log(val)
        this.setState({
            name: val
        })
    }
    render () {
        return (
            <div>
                <div>11{this.state.name}</div>
                <a onClick={this.deal.bind(this,'12123')}>click it</a>
            </div>
        )
    }
}
export default Home