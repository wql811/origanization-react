import React, { Component } from 'react'
import { is, fromJS } from 'immutable'

import { loginRule } from '../../utils/rules.js'
import { Form, Input, Button, Checkbox, Layout } from 'antd'
import './login.css'

class Login extends Component {
    state= {
        userName: '',
        password: '',
        pwdMD5: ''
    }
    shouldComponentUpdate (nextState) {

    }
    
    render (){
        const onFinish = (params) => {
            console.log(params)
        }
        const onFinishFailed = errorInfo => {
            console.log('Failed:', errorInfo);
        };
        return (
            <div className="login-content">
                <div className="login-form">
                    <h3 className="login-title">用户管理系统</h3>
                    <Form name="basic"
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}>
                        <Form.Item 
                            label="账号"
                            name="userName"
                            rules={[loginRule.userName]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item 
                            label="密码"
                            name="password"
                            rules={[loginRule.password]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item >
                            <Button type="primary" htmlType="submit" className="login-btn">登录</Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        )
    }
}
export default Login